import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {mergeMap, Observable, of, map, first, tap} from "rxjs";
import {PostService} from "./post.service";

@Injectable()
export class PostResolver implements Resolve<boolean> {
  constructor(private PostService: PostService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.PostService.loaded$.pipe(
      tap(loaded => {
        if (!loaded) {
          this.PostService.getAll();
        }
      }),
      first()
    );
  }

}
