import {Injectable} from '@angular/core';
import {DefaultDataService, HttpUrlGenerator} from "@ngrx/data";
import {Post} from "../models/post.model";
import {HttpClient} from "@angular/common/http";
import {map, Observable, tap} from "rxjs";
import {Update} from "@ngrx/entity";

@Injectable()
export class PostDataService extends DefaultDataService<Post> {

  constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator) {
    super('Post', http, httpUrlGenerator);
  }
  override getAll(): Observable<Post[]> {
    return this.http.get<Array<Post>>(`http://localhost:3000/posts`)
      .pipe(
        //tap(e=> { console.log(e)} ),
        map((resp:Array<Post>) => {
          const posts: Post[] = [];
          console.log(resp);

          // resp.forEach((e,k) => {
          //   console.log(e);
          //    console.log(k)
          //   posts.push({...e, my_index: k.toString()});
          // });
          // for (let [e,r] in resp) {  posts.push({...resp[p], id: p});  }
          return resp;
        })
      );
  }

  override add(post: Post): Observable<Post> {
    return this.http
      .post<{ name: string }>(`http://localhost:3000/posts`, post)
      .pipe(map((data) => {
          return {...post, id: data.name};
        })
      );
  }


  override update(post: Update<Post>): Observable<Post> {
    return this.http.put<Post>(`http://localhost:3000/posts/${post.id}`, {...post.changes});
  }
}
