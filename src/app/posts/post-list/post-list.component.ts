import {Component, OnInit} from '@angular/core';
import {PostService} from "../post.service";
import {Observable} from "rxjs";
import {Post} from "../../models/post.model";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  posts$: Observable<Post[]>;

  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.posts$ = this.postService.entities$;
    //this.posts$ = this.postService.getAll();
  }

}
