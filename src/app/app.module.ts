import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PostListComponent} from './posts/post-list/post-list.component';
import {SinglePostComponent} from './posts/single-post/single-post.component';
import {EditPostComponent} from './posts/edit-post/edit-post.component';
import {AddPostComponent} from './posts/add-post/add-post.component';
import {HomeComponent} from './home/home.component';
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {environment} from "../environments/environment";
import {EntityDataModule, EntityDataService, EntityServices} from "@ngrx/data";
import {entityConfig} from "./entity-metadata";
import {HttpClientModule} from "@angular/common/http";
import {PostDataService} from "./posts/post-data.service";
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {PostResolver} from "./posts/post.resolver";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    SinglePostComponent,
    EditPostComponent,
    AddPostComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument({logOnly: environment.production,
    }),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot(entityConfig),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [PostDataService,PostResolver],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    entityDataService: EntityDataService,
    postsDataService: PostDataService
    ) {
    entityDataService.registerService('Post', postsDataService)
  }
}
